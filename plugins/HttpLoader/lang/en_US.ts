<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>HttpLoader</name>
    <message>
        <location filename="../httploader.cpp" line="66"/>
        <source>HttpLoader</source>
        <translation>HttpLoader</translation>
    </message>
    <message>
        <source>Sarvaritdino R.</source>
        <translation type="obsolete">Sarvaritdinov R.</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="67"/>
        <source>Sarvaritdinov R.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="73"/>
        <source>Плагин для скачивания файлов по протоколу HTTP/S.</source>
        <translation>Plugin for downloading files via HTTP/S.</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="348"/>
        <source>Установлены границы секции с %1 байта по %2 байт</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="349"/>
        <source>Установлено смещение в секции на %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="496"/>
        <source>Секция завершена</source>
        <translation>Section completed</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="527"/>
        <source>Проверка размера задания</source>
        <translation>Checking task size</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1114"/>
        <source>400 Bad Request</source>
        <translation>400 Bad Request</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1115"/>
        <source>401 Unauthorized</source>
        <translation>401 Unauthorized</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1116"/>
        <source>403 Forbidden</source>
        <translation>403 Forbidden</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1117"/>
        <source>407 Proxy Authentication Required</source>
        <translation>407 Proxy Authentication Required</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1118"/>
        <source>409 Conflict</source>
        <translation>409 Conflict</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1119"/>
        <source>410 Gone</source>
        <translation>410 Gone</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1120"/>
        <source>414 Request-URL Too Long</source>
        <translation>414 Request-URL Too Long</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1121"/>
        <source>500 Internal Server Error</source>
        <translation>500 Internal Server Error</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1122"/>
        <source>501 Not Implemented</source>
        <translation>501 Not Implemented</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1123"/>
        <source>502 Bad Gateway</source>
        <translation>502 Bad Gateway</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1124"/>
        <source>503 Service Unavailable</source>
        <translation>503 Service Unavailable</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1125"/>
        <source>504 Gateway Timeout</source>
        <translation>504 Gateway Timeout</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1126"/>
        <source>505 HTTP Version Not Supported</source>
        <translation>505 HTTP Version Not Supported</translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1127"/>
        <source>Файл больше недоступен по данному URL</source>
        <translation>File on this URL is no longer available </translation>
    </message>
    <message>
        <location filename="../httploader.cpp" line="1129"/>
        <source>Неизвестная ошибка. Код ошибки = </source>
        <translation>Unknown Error. Error code = </translation>
    </message>
</context>
<context>
    <name>HttpSection</name>
    <message>
        <location filename="../httpsection.cpp" line="36"/>
        <source>Создана секция</source>
        <translation>Section created</translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="125"/>
        <source>Попытка соединения с %1 на порту %2</source>
        <translation>Trying to connect to %1 on port %2</translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="140"/>
        <source>Установлено смещение в секции на %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="151"/>
        <source>Установлены границы секции с %1 байта по %2 байт</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="260"/>
        <source>Секция остановлена</source>
        <translation>Section stopped</translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="278"/>
        <source>Соединение с узлом установлено</source>
        <translation>Connected to host</translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="312"/>
        <source>Отправка HTTP заголовка</source>
        <translation>Sending HTTP header</translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="343"/>
        <source>Получен ответ %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="391"/>
        <source>Докачка не поддерживается</source>
        <translation>Resume is not supported</translation>
    </message>
    <message>
        <location filename="../httpsection.cpp" line="413"/>
        <source>Докачка поддерживается</source>
        <translation>Resume is supported</translation>
    </message>
</context>
</TS>
